import { combineReducers } from "redux";
import searchReducer from "./searchReducer";
import { SearchState } from './searchReducer';

export interface RootStore {
  movies: SearchState
}

export default combineReducers({
  movies: searchReducer
});
