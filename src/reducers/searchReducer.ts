import {SEARCH_MOVIE, FETCH_MOVIES, FETCH_MOVIE, LOADING} from "../actions/types";
import {FetchMoviesResponse} from "../components/SearchForm";
import {Movie} from "../components/Movie";

export interface SearchState  {
  readonly text: string;
  readonly movies: any | FetchMoviesResponse;
  readonly loading: boolean;
  readonly movie: Movie<string> | any;
}

interface SearchAction<T>  {
  readonly type: string;
  readonly payload?: T;
}

const initialState: SearchState = {
  text: "",
  movies: {},
  loading: false,
  movie: {}
};

export default <T>(state = initialState, action: SearchAction<T>) => {
  switch (action.type) {
    case SEARCH_MOVIE:
      return {
        ...state,
        text: action.payload,
        loading: false
      };

    case FETCH_MOVIES:
      return {
        ...state,
        movies: action.payload,
        loading: false
      };

    case FETCH_MOVIE:
      return {
        ...state,
        movie: action.payload,
        loading: false
      };

    case LOADING:
      return {
        ...state,
        loading: true
      };

    default:
      return state;
  }
};
