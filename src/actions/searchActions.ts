import { Dispatch } from "redux";
import { APIKey } from '../APIKey';
import {FETCH_MOVIES, FETCH_MOVIE, SEARCH_MOVIE, LOADING} from './types';

export const searchMovie = (text: string) => (dispatch: Dispatch) => {
  dispatch({
    type: SEARCH_MOVIE,
    payload: text
  });
};

export const fetchMovies = (text: string) => (dispatch: Dispatch) => {
  fetch(`http://www.omdbapi.com/?apikey=${APIKey}&s=${text}`)
      .then(r => r.json().then(json => r.ok ? json : Promise.reject(json)))
      .then(r => dispatch({
        type: FETCH_MOVIES,
        payload: r
      }))
      .catch(err => console.error(err))
};

export const fetchMovie = (id?: string) => (dispatch: Dispatch) => {
    if (id) {
        fetch(`http://www.omdbapi.com/?apikey=${APIKey}&i=${id}`)
            .then(r => r.json().then(json => r.ok ? json : Promise.reject(json)))
            .then(r => dispatch({
                type: FETCH_MOVIE,
                payload: r
            }))
            .catch(err => console.error(err))
    }
};

export const setLoading = () => ({
    type: LOADING
})