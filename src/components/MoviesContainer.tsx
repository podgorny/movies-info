import React from "react";
import { RootStore } from '../reducers';
import {useSelector} from 'react-redux';
import MovieCard from "./MovieCard";
import {FetchMoviesResponse} from "./SearchForm";

interface StateProps {
    movies: FetchMoviesResponse
}

const MoviesContainer: React.FC = () => {
    const {movies} = useSelector<RootStore, StateProps>((state) => ({
        movies: state.movies.movies
    }));

    let content = movies.Response === 'True' ? movies.Search.map((movie, i) => <MovieCard key={i} movie={movie}/>) :
        movies.Response ? (
        <div className='container'>
            <div className="alert alert-info text-center" role="alert">
                Nothing found ¯\_(ツ)_/¯
            </div>
        </div>
    ) : null;

    return (
        <div className="row">
            {content}
        </div>
    );
};

export default MoviesContainer;

