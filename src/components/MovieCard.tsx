import React from "react";
import {Link} from "react-router-dom";

export interface MovieConcise {
    Poster: string
    Title: string
    Year: number
    imdbID: string
}

interface Props {
    readonly movie: MovieConcise
}

const MovieCard: React.FC<Props> = ({movie}) => {

    return (
        <div className="col-md-3 mb-5">
            <div className="card card-body bg-dark text-center h-100 d-flex flex-column justify-content-between">
                <div className='d-flex flex-column justify-content-center align-items-center'>
                    <img className="w-100 h-65 mb-2" src={movie.Poster} alt="Movie Cover"/>
                </div>

                <div>
                    <h5 className="text-light card-title">
                    <span className='d-block text-truncate w-100'>
                    {movie.Title}
                    </span>
                        ({movie.Year})
                    </h5>
                    <Link className="btn btn-primary" to={`/movie/${movie.imdbID}`}>
                        Movie Details
                        <i className="ml-2 fas fa-chevron-right movie__details-arrow"/>
                    </Link>
                </div>
            </div>
        </div>
    );
};

export default MovieCard;

