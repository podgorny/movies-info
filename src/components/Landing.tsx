import React from "react";
import SearchForm from "./SearchForm";
import {useSelector} from 'react-redux';
import {RootStore} from '../reducers';
import MoviesContainer from "./MoviesContainer";

interface StateProps {
    loading: boolean
}

const Landing: React.FC = () => {
    const {loading} = useSelector<RootStore, StateProps>((state) => ({
        loading: state.movies.loading
    }));

    return (
        <div className="container">
            <SearchForm/>
            {loading ? (
                <div className='d-flex align-items-center justify-content-center'>
                    <div className="spinner-grow text-white" role="status">
                        <span className="sr-only">Loading...</span>
                    </div>
                </div>
            ) : <MoviesContainer/>}
        </div>
    );
};

export default Landing;
