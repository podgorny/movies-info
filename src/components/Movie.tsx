import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {RootStore} from '../reducers'
import {fetchMovie, setLoading} from "../actions/searchActions";
import {useParams} from "react-router";
import {Link} from "react-router-dom";

interface MovieRatings<T> {
    Source: T
    Value: T
}

export interface Movie<T> {
    Actors: T
    Awards: T
    BoxOffice: T
    Country: T
    DVD: T
    Director: T
    Genre: T
    Language: T
    Metascore: T
    Plot: T
    Poster: T
    Production: T
    Rated: T
    Ratings: Array<MovieRatings<string>>
    Released: T
    Response: T
    Runtime: T
    Title: T
    Writer: T
    Year: T
    imdbID: T
    imdbRating: T
    imdbVotes: T
}

interface Props {
    loading: boolean
    movie: Movie<string> | any
}

const Movie: React.FC = () => {

    const {loading, movie} = useSelector<RootStore, Props>(state => ({
        loading: state.movies.loading,
        movie: state.movies.movie
    }));

    const dispatch = useDispatch();

    const { id } = useParams();

    useEffect(() => {
        dispatch(setLoading());
        dispatch(fetchMovie(id))
    }, []);

    const movieInfo = (
        <div className="container pt-4">
            <div className="row">
                <div className="col-md-4 card card-body">
                    <img src={movie.Poster} className="thumbnail" alt="Poster"/>
                </div>
                <div className="col-md-8">
                    <h2 className="mb-4 text-light">{movie.Title}</h2>
                    <ul className="list-group">
                        <li className="list-group-item">
                            <strong>Genre:</strong> {movie.Genre}
                        </li>
                        <li className="list-group-item">
                            <strong>Released:</strong> {movie.Released}
                        </li>
                        <li className="list-group-item">
                            <strong>Rated:</strong> {movie.Rated}
                        </li>
                        <li className="list-group-item">
                            <strong>IMDB Rating:</strong> {movie.imdbRating}
                        </li>
                        <li className="list-group-item">
                            <strong>Director:</strong> {movie.Director}
                        </li>
                        <li className="list-group-item">
                            <strong>Writer:</strong> {movie.Writer}
                        </li>
                        <li className="list-group-item">
                            <strong>Actors:</strong> {movie.Actors}
                        </li>
                    </ul>
                </div>
            </div>
            <div className="row">
                <div className="card card-body bg-dark my-5 text-light">
                    <div className="col-md-12">
                        <h3>About </h3>
                        {movie.Plot}
                        <hr/>
                        <a
                            href={'https://www.imdb.com/title/' + movie.imdbID}
                            target="_blank"
                            rel="noopener noreferrer"
                            className="btn btn-primary"
                        >
                            View on IMDB
                        </a>
                        <Link to="/" className="btn btn-default text-light">
                            Go Back To Search
                        </Link>
                    </div>
                </div>
            </div>
        </div>
    );

    let content = loading ? (
        <div className='d-flex align-items-center justify-content-center'>
            <div className="spinner-grow text-white" role="status">
                <span className="sr-only">Loading...</span>
            </div>
        </div>
    ) : movieInfo;

    return (
        <div>
            {content}
        </div>
    );
};

export default Movie;

