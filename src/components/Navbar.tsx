import React from "react";
import {Link} from "react-router-dom";

const Navbar: React.FC = () => {
  return (
    <header>
      <nav className="navbar navbar-dark bg-dark pt-3 pb-3 pr-4 pl-4">
        <Link className="navbar-brand display-1" to="/">
          <i className="fas fa-film mr-2" />
          MoviesInfo
        </Link>
        <div>
          <i className="fab fa-react fa-2x mr-3 text-info" />
          <i className="fab fa-imdb fa-2x text-warning" />
        </div>
      </nav>
    </header>
  );
};

export default Navbar;
