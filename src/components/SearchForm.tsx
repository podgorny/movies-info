import React from "react";
import { useSelector, useDispatch } from "react-redux";

import {searchMovie, fetchMovies, setLoading} from "../actions/searchActions";
import { RootStore } from '../reducers';
import {MovieConcise} from "./MovieCard";

export interface FetchMoviesResponse {
  Search: Array<MovieConcise>
  totalResults: string
  Response: string
}

interface StateProps {
  text: string;
  loading: boolean
}

const SearchForm: React.FC = () => {
  const dispatch = useDispatch();

  const { text, loading } = useSelector<RootStore, StateProps>(state => ({
    text: state.movies.text,
    loading: state.movies.loading
  }));

  const onChange: React.ChangeEventHandler<HTMLInputElement> = e => {
    dispatch(searchMovie(e.target.value));
  };

  const onSubmit: React.FormEventHandler<HTMLFormElement> = e => {
    e.preventDefault();
    dispatch(setLoading());
    dispatch(fetchMovies(text));
  };

  return (
    <div className="jumbotron jumbotron-fluid mt-5 text-center rounded-lg">
      <div className="container">
        <h1 className="display-4 mb-3">
          <span>Search for a movie ,TV series ..</span>
        </h1>
        <form id="searchForm" onSubmit={onSubmit}>
          <div className="input-group input-group-lg">
            <div className="input-group-prepend">
              <span className="input-group-text" id="inputGroup-sizing-lg">
                <i className="fas fa-search align-middle" />
              </span>
            </div>
            <input
              type="search"
              className="form-control"
              name="searchText"
              aria-label="Sizing example input"
              aria-describedby="inputGroup-sizing-lg"
              placeholder="Search Movies, TV Series ..."
              onChange={onChange}
              value={text}
              autoComplete='off'
            />
          </div>
          <button type="submit" className="btn btn-primary btn-bg btn-lg mt-3" disabled={loading}>
            Search
          </button>
        </form>
      </div>
    </div>
  );
};

export default SearchForm;
