import React from "react";
import Navbar from "./components/Navbar";
import Landing from "./components/Landing";

import {Provider} from "react-redux";

import store from "./store";
import {HashRouter, Route} from "react-router-dom";
import Movie from "./components/Movie";

function App() {
    return (
        <Provider store={store}>
            <HashRouter>
                <Navbar/>
                    <Route path='/' component={Landing} exact />
                    <Route path='/movie/:id' component={Movie} exact />
            </HashRouter>
        </Provider>
    );
}

export default App;
